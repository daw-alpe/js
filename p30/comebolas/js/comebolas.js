 this.g = {};

 g.v = 1; //contador para saber cúantos fantasmas hay que colocar
 g.comida = 0; //verdes que te comes
 g.puntuación = 0;
 g.salir=true; //salir del bucle al colisionar con un fantasma rojo

 //objeto comecocos
 g.pacoco = {
     x: 0,
     y: 0
 };



 //precargar de imágenes

 //fantasmas
 var verdes = new Image();
 verdes.src = "imgs/cocoVerde.png";
 var rojos = new Image();
 rojos.src = "imgs/cocoRojo.png";


 //comecocos
 var cocos = new Array();
 for (var c = 0; c < 4; c++) {
     cocos[c] = new Image();
     cocos[c].src = "imgs/" + [c] + ".png";
 }

 function dibujarCanvas() {
     g.canvas = document.getElementById('miCanvas');
     g.contexto = g.canvas.getContext('2d');
     g.buenos = new Array();
     g.malos = new Array();
     colocarFantasmas(g.v);
     f1();
     window.addEventListener("keydown", f2);
 }

 function f2() {
     //console.log(event.which);
     switch (event.which) {
         case 38:
             g.direccion = "arriba";

             break;
         case 40:
             g.direccion = "abajo";

             break;
         case 37:
             g.direccion = "izquierda";

             break;
         case 39:
             g.direccion = "derecha";

             break;
     }
 }

 function f1() {
     g.bucle = setTimeout(function() { window.requestAnimationFrame(f1) }, 5);
     //window.requestAnimationFrame(f1); 

     //hacer que rebote cuadradito
     if (g.pacoco.y < 0) {
         g.direccion = "abajo";

     } else if (g.pacoco.y > 500) {
         g.direccion = "arriba";
     }

     if (g.pacoco.x < 0) {
         g.direccion = "derecha";
     } else if (g.pacoco.x > 800) {
         g.direccion = "izquierda";
     }

     limpiar();

     switch (g.direccion) {
         case "arriba":
             g.contexto.drawImage(cocos[3], g.pacoco.x, g.pacoco.y);
             g.pacoco.y -= 1;
             break;
         case "abajo":
             g.contexto.drawImage(cocos[1], g.pacoco.x, g.pacoco.y);
             g.pacoco.y += 1;
             break;
         case "izquierda":
             g.contexto.drawImage(cocos[2], g.pacoco.x, g.pacoco.y);
             g.pacoco.x -= 1;
             break;
         case "derecha":
             g.contexto.drawImage(cocos[0], g.pacoco.x, g.pacoco.y);
             g.pacoco.x += 1;
             break;
         default:
             g.contexto.drawImage(cocos[0], g.pacoco.x, g.pacoco.y);
             //g.contexto.drawImage(coco,200, 180);
             break;
     }

     if(g.salir==true){
         for (var c = 0; c < g.v; c++) {
             g.contexto.drawImage(verdes, g.buenos[c].xV, g.buenos[c].yV);
             g.contexto.drawImage(rojos, g.malos[c].xR, g.malos[c].yR);
             colision(g.malos[c]);
             colision(g.buenos[c]);
         }
     }else{
        for (var c = 0; c < g.v; c++) {
             g.contexto.drawImage(verdes, g.buenos[c].xV, g.buenos[c].yV);
             g.contexto.drawImage(rojos, g.malos[c].xR, g.malos[c].yR);
         }
        window.removeEventListener("keydown", f2); //no hace nada
        clearTimeout(g.bucle); //esto para a pacoco
     }
 }

 /*
    Función que recibe como argumento el número de fantasmas de cada color que se deben 
    colocar en la pantalla de cada nivel
  */
 function colocarFantasmas(v) {

     for (var c = 0; c < v; c++) {
         var xV = Math.floor(Math.random() * 801);
         var yV = Math.floor(Math.random() * 501);
         g.buenos.push({ xV, yV });

         var xR = Math.floor(Math.random() * (801 - 0));
         var yR = Math.floor(Math.random() * (501 - 0));
         g.malos.push({ xR, yR });
     }

 }

 /*
     Función que recibe como argumento el objeto fantasma a analizar y comprueba si 
     las coordenadas de ese objeto coinciden con la posición del comecocos
      Si el fantasma es verde y coinciden posiciones:
         el fantasma desaparece
         suma uno a la puntuación
         aumenta g.v
      Si el fantasma es rojo
         GAME OVER
  */
 function colision(bicho) {
     if (((g.pacoco.x + 50 >= bicho.xR) && (g.pacoco.x < bicho.xR + 50)) && ((g.pacoco.y + 50 >= bicho.yR) && (g.pacoco.y <= bicho.yR + 55))) {
          g.salir=false;
         looser();
     }

     if (((g.pacoco.x + 50 >= bicho.xV) && (g.pacoco.x < bicho.xV + 50)) && ((g.pacoco.y + 50 >= bicho.yV) && (g.pacoco.y <= bicho.yV + 55))) {
         g.comida++;
         //esconder fantasma verde
         bicho.xV = -60;
         bicho.yV = -60;
         //cuando comas todos los verdes, se cambia de pantalla, aumentando el nº de fantasmas 
         //e inicialinzando la comida
         if (g.v == g.comida) {
             g.v++;
             dibujarCanvas();
             g.comida = 0;
         }
         //Al comer un fantasma verde se suman los puntos
         g.puntuación++;
         sumaPuntos();
     }
 }

 /* limpiar lienzo */
 function limpiar() {
     g.contexto.clearRect(0, 0, g.canvas.width, g.canvas.height);
     g.contexto.fillRect(0, 0, g.canvas.width, g.canvas.height);
 }

 function looser() {
     alert("GAME OVER");
     document.querySelector(".puntuacion+div").style.display = "block";
     document.querySelector(".boton").addEventListener("click", function() {
         location.reload();
     });
   
 }

 function sumaPuntos() {
     document.querySelector(".puntuacion span").innerHTML = g.puntuación + " puntos";
 }

 window.addEventListener("load", dibujarCanvas);