//precarga de imágenes
var imagenes = new Array();
for (var c = 65; c <= 90; c++) {
    imagenes[c] = new Image();
    imagenes[c].url = "url(./imgs/" + c + ".gif)";
}

//precarga de audio
var audio = document.createElement("audio");
audio.src = "imgs/a.m4a";


window.addEventListener("load", function() {
    window.addEventListener("keydown", encender);
    window.addEventListener("keydown", function() {
        audio.play();
    });
    window.addEventListener("keyup", function() {
        audio.pause();
    });
});

function encender(event) {
    document.querySelector(".contenedor").style.backgroundImage = imagenes[event.which].url;
}