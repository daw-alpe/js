//precarga de imágenes
var imagenes = new Array();
for (var c = 65; c <= 90; c++) {
    imagenes[c] = new Image();
    imagenes[c].url = "url(./imgs/" + c + ".gif)";
}

//objeto iluminado array de codigos
var iluminacion = {
    fotos: new Array(),
    retardo: 500,
    ultima: 0, 
    cambio:""
};


window.addEventListener("load", function() {

    document.querySelector("#boton").addEventListener("click", function() {
        var frase = document.querySelector("#caja").value;
        frase = frase.toUpperCase(); //para que charCodeAt sea = que which
        frase = frase.split("");
   
        var codigo = 0;

        for (var c = 0; c < frase.length; c++) {
            //quitar espacios en blanco
            if (frase[c] != " ") {
                //crear array de código de letras
                //fotos.push(frase[c].charCodeAt(0));
                codigo = frase[c].charCodeAt(0);

               iluminacion.fotos.push(codigo);

            }
        }
        iluminacion.cambio=setInterval(function(){encender(iluminacion.fotos)}, iluminacion.retardo);
    });
    
    
});




function encender() {
    var fondo = document.querySelector(".contenedor>div>div");
    
    fondo.style.backgroundImage = imagenes[iluminacion.fotos[iluminacion.ultima%iluminacion.fotos.length]].url;
    iluminacion.ultima++;
    if(iluminacion.ultima==iluminacion.fotos.length+1){
    	clearInterval(iluminacion.cambio);
    	location.reload();
    }

}

