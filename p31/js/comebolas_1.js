 this.g = {};
 g.x = 0;
 g.y = 0;
 g.v = 1; //contador para saber cúantos fantasmas hay que colocar

 g.buenos = new Array();
 g.malos = new Array();

 //precargar de imágenes
 
 //fantasmas
 var verdes = new Image();
 verdes.src = "imgs/cocoVerde.png";
 var rojos = new Image();
 rojos.src = "imgs/cocoRojo.png";


 //comecocos
 var cocos = new Array();
 for (var c = 0; c < 4; c++) {
     cocos[c] = new Image();
     cocos[c].src = "imgs/" + [c] + ".png";
 }

 function dibujarCanvas() {
     g.canvas = document.getElementById('miCanvas');
     g.contexto = g.canvas.getContext('2d');
     colocarFantasmas(g.v);
     f1();
     window.addEventListener("keydown", f2);
 }

 function f2() {
     //console.log(event.which);
     switch (event.which) {
         case 38:
             g.direccion = "arriba";

             break;
         case 40:
             g.direccion = "abajo";

             break;
         case 37:
             g.direccion = "izquierda";

             break;
         case 39:
             g.direccion = "derecha";

             break;
     }
 }

 function f1() {
     setTimeout(function() { window.requestAnimationFrame(f1) }, 5);
     //window.requestAnimationFrame(f1); 

     //hacer que rebote cuadradito
     if (g.y < 0) {
         g.direccion = "abajo";


     } else if (g.y > 500) {
         g.direccion = "arriba";
     }

     if (g.x < 0) {
         g.direccion = "derecha";
     } else if (g.x > 800) {
         g.direccion = "izquierda";
     }

     g.contexto.clearRect(0, 0, g.canvas.width, g.canvas.height);

     g.contexto.fillRect(0, 0, g.canvas.width, g.canvas.height);



     switch (g.direccion) {
         case "arriba":
             g.contexto.drawImage(cocos[3], g.x, g.y);
             g.y -= 1;
             break;
         case "abajo":
             g.contexto.drawImage(cocos[1], g.x, g.y);
             g.y += 1;
             break;
         case "izquierda":
             g.contexto.drawImage(cocos[2], g.x, g.y);
             g.x -= 1;
             break;
         case "derecha":
             g.contexto.drawImage(cocos[0], g.x, g.y);
             g.x += 1;
             break;
         default:
             g.contexto.drawImage(cocos[0], g.x, g.y);
             //g.contexto.drawImage(coco,200, 180);
             break;
     }
     for (var c = 0; c < g.v; c++) {
         g.contexto.drawImage(verdes, g.buenos[c].xV, g.buenos[c].yV);
         g.contexto.drawImage(rojos, g.malos[c].xR, g.malos[c].yR);
         colision(g.malos[c]);
     }
 }

 /*
    Función que recibe como argumento el número de fantasmas de cada color que se deben 
    colocar en la pantalla de cada nivel
  */
 function colocarFantasmas(v) {

     for (var c = 0; c < v; c++) {
         var xV = Math.floor(Math.random() * (801 - 0));
         var yV = Math.floor(Math.random() * (501 - 0));
         g.buenos.push({ xV, yV });
        
         var xR = Math.floor(Math.random() * (801 - 0));
         var yR = Math.floor(Math.random() * (501 - 0));
         g.malos.push({ xR, yR });
     }

 }

/*
    Función que recibe como argumento el objeto fantasma a analizar y comprueba si 
    las coordenadas de ese objeto coinciden con la posición del comecocos
     Si el fantasma es verde y coinciden posiciones:
        el fantasma desaparece
        suma uno a la puntuación
        aumenta g.v
     Si el fantasma es rojo
        GAME OVER
 */
 function colision(bicho){
    if(bicho==g.malos[0]){
        if(((g.pacoco.x+50>=bicho.xR) && (g.pacoco.x<bicho.xR+50)) && ((g.pacoco.y+50>=bicho.yR)&&(g.pacoco.y<=bicho.yR+55))){
            alert("Game OVER");
          }
    }

     
 }

 window.addEventListener("load", dibujarCanvas);