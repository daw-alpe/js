/*
	Escribir un programa que lea una frase y determine la frecuencia de aparición de cada
		vocal con respecto al total de caracteres de la frase.
 */

//Cargar página
//Llamar a función que cuenta las vocales
window.addEventListener("load",function(){
	contarVocales();
});

/*
	Función que cuenta vocales con relación al total de caracteres
	Llama a función mostrar
 */
function contarVocales(){
	var cadena="El gato que come girasoles";
	cadena=cadena.toLowerCase();
	var cadenaSin="";

	//quitar espacios
	for(var i=0; i<cadena.length; i++){
		if(cadena.charAt(i)!=" "){
			cadenaSin+=cadena.charAt(i);
		}
	}
	
		
	//variables que almacenan vocales y longitud
	var longitud=cadenaSin.length;
	var a=0;
	var e=0;
	var i=0;
	var o=0;
	var u=0;

	//Contar vocales
	for(var c=0; c<cadenaSin.length; c++){
		switch (cadenaSin.charAt(c)){
			case "a":
				a++;
				break;
			case "e":
				e++;
				break;
			case "i":
				i++;
				break;
			case "o":
				o++;
				break;
			case "u":
				u++;
				break;
		}
	}

	mostrar(cadena, longitud, a, e, i, o, u);
}

/*
	Función que recibe 6 argumentos
		Frase sobre la que se han realizado las operaciones
		Longitud de esa frase
		Las frecuencias de las 5 vocales
	lo muestra por pantalla en dos div diferentes
 */
function mostrar(txt, long, a, b, c, d, e){
	var cajas=document.querySelectorAll(".contenedor>div");
	cajas[0].innerHTML='"'+txt.toUpperCase()+'"';
	cajas[1].innerHTML="Total de caracteres: " + long + "<br>A: " + a + " veces<br>E: "+ 
		b + " veces<br>I: "+ c + " veces<br>O: "
		+ d + " veces<br>U: " +  e + " veces."
}