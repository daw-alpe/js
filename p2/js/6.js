/*
	Escribir un programa que lea del teclado una cadena y muestre en la pantalla
	 la cantidad de consonantes y vocales que contiene.
*/

//Cargar página
//Solicitar frase a analizar
//Llamar a función que cuenta las vocales y consonantes
window.addEventListener("load",function(){
	var frase=prompt("Introduce la frase a analizar");
	frase=frase.toLowerCase();
	contarLetras(frase);
});

/*
	Función que recibe como argumento la cadena introducida a analizar
	Cuenta vocales y consonantes 
	Llama a función mostrar
 */
function contarLetras(cadena){
	//quitar espacios
	var cadenaSin="";
	for(var i=0; i<cadena.length; i++){
		if(cadena.charAt(i)!=" "){
			cadenaSin+=cadena.charAt(i);
		}
	}

	//variables que almacenan vocales y consonantes
	var vocales=new Array("a", "e", "i", "o", "u");
	var consonantes=0;
	var contVocales=0;
	
	//Contar vocales
	for(var c=0; c<cadenaSin.length; c++){
		for(var i=0; i<vocales.length; i++){
			if(cadenaSin.charAt(c)==vocales[i]){
				contVocales++;
			}
		}
	}

	//Contar consonantes
	consonantes=cadenaSin.length-contVocales;

	mostrar(cadena, contVocales, consonantes);
}

/*
	Función que recibe 3 argumentos
		Frase sobre la que se han realizado las operaciones
		El número de vocales que contiene
		El número de consonantes que contiene
	lo muestra por pantalla en dos div diferentes
 */
function mostrar(txt, vocal, letra){
	var cajas=document.querySelectorAll(".contenedor>div");
	cajas[0].innerHTML='"'+txt.toUpperCase()+'"';
	cajas[1].innerHTML="Hay " + vocal + " vocales<br>Hay " + letra + " consonantes";
}