/*
	Escribir un programa que lea del teclado una cadena y construya y 
	muestre en la pantalla otra cadena en la que cada vocal 
	haya sido reemplazada por un punto.
*/

//Cargar página
//Solicitar frase a analizar
//Llamar a función que cuenta lreemplace las vocales 
window.addEventListener("load",function(){
	var frase=prompt("Introduce la frase a analizar");
	frase=frase.toLowerCase();
	cambiarVocales(frase);
});

/*
	Función que recibe como argumento la cadena introducida a analizar
	Crea una cadena nueva igual a la anterior
	Cambia las vocales por punto 
	Llama a función mostrar
 */
function cambiarVocales(cadena){
	var texto=cadena;
	var vocales=new Array("a", "e", "i", "o", "u");

	//Cambiar vocales por puntos
	var texto=texto.split("");
	for(var c=0; c<texto.length; c++){
		for(var i=0; i<vocales.length; i++){
			if(texto[c]==vocales[i]){
				texto[c]=".";
			}
		}
	}

	mostrar(cadena, texto);
}

/*
	Función que recibe 2 argumentos
		Frase sobre la que se han realizado las operaciones
		Nueva frase con vocales cambiadas
	lo muestra por pantalla en dos div diferentes
 */
function mostrar(txt, sinVocales){
	var cajas=document.querySelectorAll(".contenedor>div");
	cajas[0].innerHTML='"'+txt.toUpperCase()+'"';
	cajas[1].innerHTML="Nueva cadena: " + sinVocales.join("");
}