//precarga audio
var audio = new Array();
var notas = new Array("DO", "RE", "MI", "FA", "SOL", "LA", "SI", "DO2");
for (var c = 0; c < notas.length; c++) {
    audio[c] = document.createElement("audio");
    audio[c].src = "media/" + notas[c] + ".mp3";
}

//variables globales 
this.g={};
g.num;
g.teclas;

//tras cargar la página asignar el evento click a las teclas
window.addEventListener("load", function() {
    g.teclas = document.querySelectorAll(".contenedor>div>div>div");
    for (var c = 0; c < g.teclas.length; c++) {
        g.teclas[c].addEventListener("click", tocar);
    }
});

/*
	Función que recoge evento de tecla pulsada y asigna nota concreta
 */
function tocar(event) {
    var atributo=event.target.getAttribute("data-id");
    if(!atributo){
        atributo=event.target.parentNode.getAttribute("data-id");
        //event.target.style.boxShadow="0px 0px 0px 0px rgba(0,0,0,0)";
    }
        
    if (g.num != undefined) {
        if (g.num == atributo) {
            //parar sonido de la primera pulsación de una misma nota tocada seguida
            audio[g.num].pause();
        } else {
            //quitar sombreado de teclas pulsadas antes
            g.teclas[g.num].style.boxShadow = "none";
        }
    }

    g.num = atributo;
    
    if(event.target.getAttribute("data-id")){
        event.target.style.boxShadow = "0px 0px 17px 6px rgba(0,0,0,0.75)";
    }else{
        event.target.parentNode.style.boxShadow = "0px 0px 17px 6px rgba(0,0,0,0.75)";
    }
    
   
    
    //iniciar el sonido desde el principio
    audio[g.num].currentTime = 0;
    audio[g.num].play();
    
}